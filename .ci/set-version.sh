#!/usr/bin/env bash
if [[ -n "$CI_COMMIT_TAG" ]]; then
  echo -e "\nsonar.projectVersion=$CI_COMMIT_TAG" >> sonar-project.properties
else
  echo -e "\nsonar.projectVersion=latest" >> sonar-project.properties
fi
