# content-negotiation-go

A Go library to perform [Content Negotiation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation).

See documentation on [pkg.go.dev](https://pkg.go.dev/gitlab.com/jamietanna/content-negotiation-go)

## Usage

```sh
go get gitlab.com/jamietanna/content-negotiation-go
```

```go
import (
	"fmt"

	contentnegotiation "gitlab.com/jamietanna/content-negotiation-go"
)

func ExampleNegotiator_Negotiate() {
	negotiator := contentnegotiation.NewNegotiator("application/json", "application/xml")
	negotiated, provided, err := negotiator.Negotiate("application/json")
	if err == nil {
		fmt.Println(negotiated.String())
		fmt.Println(provided.String())
	}
	// output:
	// application/json
	// application/json
}
```

Further examples can be found in the documentation.

## License

This code is licensed under the same terms as the Go source code, a BSD-3 Clause license.
