package contentnegotiation

import (
	"fmt"
	"math"
	"mime"
	"reflect"
	"strconv"
	"strings"
)

// MediaType represents a Media Type for use with HTTP applications, of format:
//   type/subtype
//   type/subtype;parameter=value
//   type/subtype+suffix
//
// See also: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
type MediaType struct {
	theType       string
	subType       string
	subTypeSuffix string
	params        map[string]string
}

// NewMediaType parse a given string as a MediaType, returning `nil` if there was an error with it
func NewMediaType(mediaType string) *MediaType {
	mediaType, params, err := mime.ParseMediaType(mediaType)
	if err != nil {
		return nil
	}

	parts := strings.SplitN(mediaType, "/", 2)
	if len(parts) == 1 {
		return nil
	}

	if parts[0] == "*" && !strings.HasPrefix(parts[1], "*") {
		return nil
	}

	mt := MediaType{
		theType: parts[0],
		subType: parts[1],
		params:  params,
	}

	suffixParts := strings.SplitN(mt.subType, "+", 2)
	if len(suffixParts) > 1 {
		mt.subTypeSuffix = suffixParts[1]
	}

	return &mt
}

// GetType get the type of the Media Type
func (m *MediaType) GetType() string {
	return m.theType
}

// GetSubType get the subtype of the Media Type
func (m *MediaType) GetSubType() string {
	return m.subType
}

// GetSubTypeSuffix get the subtype's suffix of the Media Type, if present, or an empty string
func (m *MediaType) GetSubTypeSuffix() string {
	return m.subTypeSuffix
}

// IsWildcardType whether the type is `*`
func (m *MediaType) IsWildcardType() bool {
	return m.theType == "*"
}

// IsWildcardSubType whether the subtype is `*`
func (m *MediaType) IsWildcardSubType() bool {
	return m.subType == "*" || strings.HasPrefix(m.subType, "*+")
}

// GetParameters get the parameters associated with the Media Type. Does not include `q` value if `q=1`
func (m *MediaType) GetParameters() map[string]string {
	q := m.GetQualityValue()
	if q == 1 {
		delete(m.params, "q")
	}
	if m.params == nil {
		return make(map[string]string)
	}
	return m.params
}

// GetQualityValue get the quality value for the given Media Type. Value is between 1 and 0, to three decimal places
func (m *MediaType) GetQualityValue() float64 {
	q, ok := m.params["q"]
	if !ok {
		return 1
	}

	parsed, err := strconv.ParseFloat(q, 32)
	if err != nil {
		return 1
	}

	if parsed > 1 || parsed < 0 {
		return 1
	}

	parsed = threeDecimalPlaces(parsed)

	return parsed
}

// IsCompatibleWith can the Media Type be deemed compatible with the `other` Media Type, based on whether either of them are wildcards, or if there is a suffix, or they are exactly the same type and suffix.
func (m *MediaType) IsCompatibleWith(other *MediaType) bool {
	if m == nil || other == nil {
		return false
	}

	if m.IsWildcardType() || other.IsWildcardType() {
		return true
	}

	if m.GetType() == other.GetType() {
		if m.GetSubType() == other.GetSubType() {
			return true
		}

		if m.IsWildcardSubType() || other.IsWildcardSubType() {
			mSuffix := m.GetSubTypeSuffix()
			otherSuffix := other.GetSubTypeSuffix()

			if m.GetSubType() == WildcardType.GetSubType() || other.GetSubType() == WildcardType.GetSubType() {
				return true
			} else if m.IsWildcardSubType() && mSuffix != "" {
				return mSuffix == other.GetSubType() || mSuffix == otherSuffix
			} else if other.IsWildcardSubType() && otherSuffix != "" {
				return m.GetSubType() == otherSuffix || otherSuffix == mSuffix
			}
		}
	}

	return false
}

// Equals whether a Media Type is an exact duplicate of the `other` Media Type, including parameters
func (m *MediaType) Equals(other *MediaType) bool {
	if m == nil || other == nil {
		return false
	}
	return m.GetType() == other.GetType() && m.GetSubType() == other.GetSubType() && reflect.DeepEqual(m.GetParameters(), other.GetParameters())
}

// String produce a string representation
func (m *MediaType) String() string {
	return mime.FormatMediaType(fmt.Sprintf("%s/%s", m.theType, m.subType), m.params)
}

// adapted from https://gosamples.dev/round-float/
func threeDecimalPlaces(val float64) float64 {
	ratio := math.Pow(10, float64(3))
	return math.Floor(val*ratio) / ratio
}
