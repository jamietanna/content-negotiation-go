package contentnegotiation

var (
	// WildcardType is the MediaType for */*
	WildcardType = MediaType{
		theType: "*",
		subType: "*",
	}
)
